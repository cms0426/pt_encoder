"""This program takes in the output text file from the watch_table.sh and graphs the table encoder value over time"""

import matplotlib.pyplot as plt
import numpy as np

x = np.array([], dtype=str)
y = np.array([], dtype=np.int32)
diff = np.array([], dtype=np.int32)
with open("199mms_out_from_198_to_0.txt", 'r') as f:
    for str in f:
        x = np.append(x, str[4:8])
        temp = int(str[12:22], 16)
        y = np.append(y, temp)

x = np.arange(0, 28.5, 0.5)

for i in range(len(y)-1):
    diff = np.append(diff, (y[i]-y[i+1]))
    if y[i+1] is None:
        continue

print(y)
print(diff)
avg = np.average(diff[10:25])
print(avg)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.grid(True)
ax.plot(x, y[:])
# fig.autofmt_xdate()
# fig.gca().set_ylim([0, 170000])
# ax.xticks(rotation=90)
plt.show()

